(function () {
    'use strict';

    angular.module('app.benefactors')
        .controller('benefactorsCtrl', ['$scope', 'utilities', 'governanceService', benefactorsCtrl]);

    function benefactorsCtrl($scope, utilities, governanceService) {
        
        governanceService.getBenefactors(function (returnData) {
            $scope.benefactors_current = returnData[0];
            $scope.benefactors_expired = returnData[1];
        });

        utilities.columnsort($scope, "votes_for", "sortColumn", "sortClass", "reverse", "reverseclass", "column");
        utilities.columnsort($scope, "votes_for", "sortColumn2", "sortClass2", "reverse2", "reverseclass2", "column2");
    }

})();
