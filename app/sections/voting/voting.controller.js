(function () {
    'use strict';

    angular.module('app.voting')
        .controller('votingCtrl', ['$scope', 'governanceService', votingCtrl]);

    function votingCtrl($scope, governanceService) {

        governanceService.getProxies(function (returnData) {
            $scope.proxies = returnData;
        });
        governanceService.getBlockproducerVotes(function (returnData) {
            $scope.blockproducers = returnData;
        });
        governanceService.getBenefactorsVotes(function (returnData) {
            $scope.benefactors = returnData;
        });
        governanceService.getDxpcoreVotes(function (returnData) {
            $scope.dxpcore = returnData;
        });
    }

})();
